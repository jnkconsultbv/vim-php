# Vim-PHP


Here is my config file for NeoVim under Ubuntu 22.04 for PHP with Prettier and autocompletion.

## Install
- sudo apt install neovim
- npm install -D @prettier/plugin-php
- copy this "init.vim" file into you ".config/nvim" folder. If the "nvim" folder doens't exist in the ".config" folder, just create it.

## Config
- start NeoVim : "nvim"
- install all the plugins in NeoVim : ":PlugInstall"
- enjoy It !

